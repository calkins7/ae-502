from stump import stumpC, stumpS
from numpy import sqrt, abs
import numpy as np
from rv2orbele import Kep2Cart

def kepler_U(dt, R, eles, a, mu, error, nMax, R0, V0):
    '''
    This function uses Newton’s method to solve the universal
    Kepler equation for the universal anomaly.

    :param dt: time since x = 0 (s)
    :param R: Position vector at t = 0
    :param eles: [a, ecc_norm, inc, RAAN, AoP, h, truAnom]
    :param a: reciprocal of the semimajor axis (1/km)
    :param mu: gravitational parameter (km**3/s**2)
    :param error: tolerable convergence error
    :param nMax: maximum allowable number of iterations
    :return:  x - the universal anomaly (km**0.5)

    Other:
    z - auxiliary variable (z = a*x**2)
    C - value of Stumpff function C(z)
    S - value of Stumpff function S(z)
    n - number of iterations for convergence
    '''
    # Get radial pos vel
    ro, vro = getRadialPosVel(R0, V0)

    #...Starting value for x:
    x  = sqrt(mu)*abs(a)*dt
    #...Iterate on Equation 3.65 until convergence occurs within the error tolerance:
    n = 0
    ratio = 1
    while abs(ratio) > error and n <= nMax:
        n = n + 1
        C = stumpC(a*x**2)
        S = stumpS(a*x**2)
        F = ro*vro/sqrt(mu)*x**2*C + (1 - a*ro)*x**3*S + ro*x - sqrt(mu)*dt
        dFdx = ro*vro/sqrt(mu)*x*(1 - a*x**2*S) + (1 - a*ro)*x**2*C + ro
        ratio = F/dFdx
        # print("n = %i, C = %.2f, S = %.2f, F = %.2f, dFdX = %.2f" % (n, C, S, F, dFdx))
        x = x - ratio

        # Check if dFdx is around 0, if so also reset guess
        # Restart loop with new initial guess if diverging
        if np.isnan(x) or abs(dFdx) < 1e-14:
            x = 0
            ratio = 1000
            n = 0


    #...Deliver a value for x, but report that nMax was reached:
    if n > nMax:
        print('\n\n**No. iterations of Kepler’s equation = %s' % n)

        print('F/dFdx = %s\n\n' % ratio)
    else:
        print("...Kepler Converged: n = %i" % n)

    return x


def rv_from_r0v0(R0, V0, t, mu, error, nMax, eles):
    '''

    :param R0: initial position vector (km)
    :param V0: initial velocity vector (km/s)
    :param t: elapsed time (s)
    :param mu: gravitational parameter (km**3/s**2)
    :return:
    R - final position vector (km)
    V - final velocity vector (km/s)
    '''
    #...Magnitudes of R0 and V0:
    r0 = np.linalg.norm(R0)
    v0 = np.linalg.norm(V0)
    #...Initial radial velocity:
    vr0 = np.dot(R0, V0)/r0
    #...Reciprocal of the semimajor axis (from the energy equation):
    alpha = 2/r0 - v0**2/mu
    #...Compute the universal anomaly:
    # (dt, R, eles, a, mu, error, nMax)
    x = kepler_U(t, r0, eles, alpha, mu, error, nMax, r0, vr0)
    #...Compute the f and g functions:
    [f, g] = f_and_g(x, t, r0, alpha, mu)
    #...Compute the final position vector:
    R = f*R0 + g*V0
    #...Compute the magnitude of R:
    r = np.linalg.norm(R)
    #...Compute the derivatives of f and g:
    [fdot, gdot] = fDot_and_gDot(x, r, r0, alpha, mu)
    #...Compute the final velocity:
    V = fdot*R0 + gdot*V0
    return R,V


def f_and_g(x, t, ro, a, mu):
    '''
    This function calculates the Lagrange f and g coefficients.
    :param x: the universal anomaly after time t (km**0.5)
    :param t: the time elapsed since ro (s)
    :param ro: the radial position at time to (km)
    :param a: reciprocal of the semimajor axis (1/km)
    :return: 
    f - the Lagrange f coefficient (dimensionless)
    g - the Lagrange g coefficient (s)
    '''
    
    z = a*x**2
    #...Equation 3.69a:
    f = 1 - x**2/ro*stumpC(z)
    #...Equation 3.69b:
    g = t - 1/sqrt(mu)*x**3*stumpS(z)
    return f, g

def fDot_and_gDot(x, r, ro, a, mu):
    '''
    This function calculates the time derivatives of the
    Lagrange f and g coefficients.
    :param x: the universal anomaly after time t (km**0.5)
    :param r: the radial position after time t (km)
    :param ro: the radial position at time to (km)
    :param a: reciprocal of the semimajor axis (1/km)
    :param mu: the gravitational parameter (km**3/s**2)
    :return: 
    fdot - time derivative of the Lagrange f coefficient (1/s)
    gdot - time derivative of the Lagrange g coefficient (dimensionless)
    '''
    
    z = a*x**2
    #...Equation 3.69c:
    fdot = sqrt(mu)/r/ro*(z*stumpS(z) - 1)*x
    #...Equation 3.69d:
    gdot = 1 - x**2/r*stumpC(z)
    
    return fdot, gdot


def chi2rv_hyperbolic(chi, orbEles, mu, F0):
    '''

    :param chi: universal variable
    :param mu: gravitational constant
    :param orbEles: [a, ecc_norm, inc, RAAN, AoP, h, truAnom]
    :return: r, v in SMA / time units
    '''

    a = orbEles[0]
    ecc = orbEles[1]

    # get F ( Curtis eqn 3.58)
    dF = chi/np.sqrt(-a)
    F = F0 + dF

    # Curtis eqn 3.44b
    truAnom = 2*np.arctan2(np.sqrt((ecc+1))*np.tanh(F / 2), np.sqrt(ecc - 1)) # rad

    # takes input as [a e i RAAN AoP h truAnom]
    # TAKES IN RADIANS
    orbEles[6] = truAnom
    r, v = Kep2Cart(orbEles, mu)

    return r, v


def chi2rv_elliptic(chi, orbEles, mu, E0):
    '''

    :param chi: universal variable
    :param mu: gravitational constant
    :param orbEles: [a, ecc_norm, inc, RAAN, AoP, h, truAnom]
    :return: r, v in SMA / time units
    '''

    a = orbEles[0]
    ecc = orbEles[1]

    # get E (Curtis 3.58)
    dE = chi/np.sqrt(a)
    E = E0 + dE

    # Curtis eqn 3.413a
    truAnom = 2*np.arctan2(np.sqrt((ecc+1))*np.tan(E / 2), np.sqrt((1-ecc))) # rad

    # takes input as [a e i RAAN AoP h truAnom]
    # TAKES IN RADIANS
    orbEles[6] = truAnom
    r, v = Kep2Cart(orbEles, mu)

    return r, v


def getRadialPosVel(R0, V0):
    """
    Gets radial pos and vel for UVP
    :param R: position vector
    :param V: velocity vector
    :return vr0: initial radial velocity
    :return r0: initial radial position
    """

    r0 = np.linalg.norm(R0)
    vr0 = np.dot(R0, V0)/np.linalg.norm(R0)


    return r0, vr0
