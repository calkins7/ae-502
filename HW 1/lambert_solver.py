from stump import stumpC, stumpS
import numpy as np
from rv2orbele import robustAcos

def lambert(R1, R2, t, string, mu):
    '''
    This function solves Lambert’s problem. Algorithm 5.2, D.25 from Curtis
    :param R1: initial position vector (km)
    :param R2: final position vector (km)
    :param t: the time of flight from R1 to R2 (a constant) (s)
    :param string: ‘pro’ if the orbit is prograde
                   ‘retro’ if the orbit is retrograde
    :param mu: gravitational parameter (km^3/s^2)
    :return: V1, V2 - initial and final velocity vectors (km/s)
    '''
    R1 = np.squeeze(np.array(R1))
    R2 = np.squeeze(np.array(R2))

    #...Magnitudes of R1 and R2:
    r1 = np.linalg.norm(R1)
    r2 = np.linalg.norm(R2)
    c12 = np.cross(np.transpose(R1), np.transpose(R2))
    theta = robustAcos(np.dot(R1.T, R2)/(r1*r2))

    #...Determine whether the orbit is prograde or retrograde:
    if string == 'pro':
        if c12[2] <= 0:
            theta = 2*np.pi - theta
    elif string == 'retro':
        if c12[2] >= 0:
            theta = 2*np.pi - theta

    #...Equation 5.35:
    A = np.sin(theta)*np.sqrt(r1*r2/(1 - np.cos(theta)))

    #...Determine approximately where F(z,t) changes sign, and
    #...use that value of z as the starting value for Equation 5.45:

    # Tabulate F(z) for z between -100 and 100
    zs = np.linspace(-100, 100, 50)
    Fzs = [F(z, t, A, mu, r1, r2) for z in zs]
    # Choose value of Fzs closest to 0
    min_ind = (np.abs(np.asarray(Fzs) - 0)).argmin()
    z = zs[min_ind]

    #...Set an error tolerance and a limit on the number of iterations:
    tol = 1.e-8
    nmax = 100

    #...Iterate on Equation 5.45 until z is determined to within the
    #...error tolerance:
    ratio = 1
    n = 0
    while (abs(ratio) > tol) and (n <= nmax):
        n = n + 1
        s = stumpS(z)
        c = stumpC(z)
        y = r1 + r2 + A * (z * s - 1) / np.sqrt(c)

        y0 = r1 + r2 + A * (0 * s - 1) / np.sqrt(c)

        Fval = (y / c) ** 1.5 * s + A * np.sqrt(y) - np.sqrt(mu) * t

        if z == 0.:
            dFdzval = np.sqrt(2) / 40 * y0 ** 1.5 + A / 8 * (np.sqrt(y0)) + A * np.sqrt(1 / 2 / y0)
        else:
            dFdzval = (y / c) ** 1.5 * (1 / 2 / z * (c - 3 * s / 2 / c) + 3 * s ** 2 / 4 / c) + A / 8 * (
                        3 * s / c * np.sqrt(y) + A * np.sqrt(c / y))

        ratio = Fval / dFdzval
        z = z - ratio

        # Restart loop with new initial guess if diverging
        if np.isnan(z):
            z = 0
            ratio = 1000


    #...Equation 5.46a:
    f = 1 - y/r1
    #...Equation 5.46b:
    g = A*np.sqrt(y/mu)
    #...Equation 5.46d:
    gdot = 1 - y/r2
    #...Equation 5.28:
    V1 = 1/g*(R2 - f*R1)
    #...Equation 5.29:
    V2 = 1/g*(gdot*R2 - R1)

    # ...Report if the maximum number of iterations is exceeded:
    if n >= nmax:
        print('\n\n **Number of Lambert Solver iterations exceeds %i \n\n' % nmax)
        return np.array([np.inf]), np.array([np.inf])
    else:
        return V1, V2


# Helper Functions
#...Equation 5.38:
def y_func(z, r1, r2, A):
    s = stumpS(z)
    c = stumpC(z)
    dum = r1 + r2 + A*(z*s - 1)/np.sqrt(c)
    return dum


#...Equation 5.40:
def F(z, t, A, mu, r1, r2):
    why = y_func(z, r1, r2, A)
    c = stumpC(z)
    s = stumpS(z)
    if why >= 0:
        dum = (why/c)**1.5*s + A*np.sqrt(why) - np.sqrt(mu)*t
    else:
        dum = np.inf
        # print("Negative y value encountered in Lambert Solver")
    return dum


#...Equation 5.43:
def dFdz(z, A, r1, r2):
    why = y_func(z, r1, r2, A)
    y0 = y_func(0, r1, r2, A)
    c = stumpC(z)
    s = stumpS(z)

    if z == 0.:
        dum = np.sqrt(2)/40*y0**1.5 + A/8*(np.sqrt(y0)) + A*np.sqrt(1/2/y0)
    else:
        dum = (why/c)**1.5*(1/2/z*(c - 3*s/2/c) + 3*s**2/4/c) + A/8*(3*s/c*np.sqrt(why) + A*np.sqrt(c/why))
    return dum


def simple_newton_raphson(A, r1, r2, t, mu, guess):
    # ...Set an error tolerance and a limit on the number of iterations:
    tol = 1e-3
    nmax = 1000

    # if y(100, r1, r2, A) >= 0:
    #     z_new = 100
    # else:
    #     z_new = -100
    z_new = guess


    diff = 1000
    n = 0
    while abs(diff) > tol and (n <= nmax):
        z_old = z_new
        if dFdz(z_old, A, r1, r2) != 0:
            z_new = z_old - F(z_old, t, A, mu, r1, r2)/dFdz(z_old, A, r1, r2)
        else:
            # fix divide by 0
            z_new = 100
            return z_new
        diff = z_old - z_new
        n = n+1

    return z_new

