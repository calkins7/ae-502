import numpy as np
from univ_var_prop import kepler_U, chi2rv_hyperbolic, chi2rv_elliptic
from rv2orbele import getE, getF
from lambert_solver import lambert
import matplotlib.pyplot as plt
import pickle
from astropy.time import Time
import os

def generate_porkchop_plot(dep_times, arr_times, r1_t0, v1_t0, r2_t0, v2_t0, t0, eles1, eles2, mu, tag, cutoff, missionTag):
    '''
    Creates porkchop plot for specified bodies

    :param dep_times: np array of departure times [time unit]
    :param arr_times: np array of arrival times [time unit]
    :param r1_t0: initial position of body 1 at epoch t0 [distance unit]
    :param v1_t0: initial velocity of body 1 at epoch t0 [distance unit / time unit]
    :param r2_t0: initial position of body 2 at epoch t0 [distance unit]
    :param v2_t0: initial velocity of body 2 at epoch t0 [distance unit / time unit]
    :param t0: initial epoch [time unit]
    :param eles1: orbital elements of body 1 in [a e i RAAN AoP h truAnom]
    :param eles2: orbital elements of body 2 in [a e i RAAN AoP h truAnom]
    :return: Figure
    '''

    num_dep = len(dep_times)
    num_arr = len(arr_times)

    # Unpack orbEles
    # [a, ecc_norm, inc, RAAN, AoP, h, truAnom]
    a1 = eles1[0] # EARTH
    a2 = eles2[0] # INTERSTELLAR
    ecc1 = eles1[1] # Earth
    ecc2 = eles2[1] # Interstellar

    # Get initial elliptical / hyperbolic mean anomaly
    # Body 1: Earth
    if ecc1 > 1:
        X01 = getF(eles1)
    else:
        X01 = getE(eles1)
   # Body 2: Interstellar
    if ecc2 > 1:
        X02 = getF(eles2)
    else:
        X02 = getE(eles2)

    # initialize output
    grid_vel = np.zeros((num_dep, num_arr))

    # initialize UVP params
    error = 1e-8
    nMax = 100

    # Loop over all dep times
    for ii in range(num_dep):
        # Loop over all arr times
        for jj in range(num_arr):
        # for jj in [5]:
            print("Running Candidate: [%i / %i] [%i / %i] ..." % (ii+1, num_dep, jj+1, num_arr))
            # get time of flight
            TOF = arr_times[jj] - dep_times[ii]

            if TOF >= 0:

                # Find state of body 1 at dep time (EARTH)
                chi_1 = kepler_U(dep_times[ii] - t0, r1_t0, eles1, 1/a1, mu, error, nMax, r1_t0, v1_t0)

                # Convert chi1 to r1
                if ecc1 > 1:
                    R1, V1 = chi2rv_hyperbolic(chi_1, eles1, mu, X01)
                else:
                    R1, V1 = chi2rv_elliptic(chi_1, eles1, mu, X01)


                # Find state of body 2 at arr time (INTERSTELLAR)
                chi_2 = kepler_U(arr_times[jj] - t0, r2_t0, eles2, 1 / a2, mu, error, nMax, r2_t0, v2_t0)

                #convert chi2 to r2
                if ecc2 > 1:
                    R2, V2 = chi2rv_hyperbolic(chi_2, eles2, mu, X02)
                else:
                    R2, V2 = chi2rv_elliptic(chi_2, eles2, mu, X02)

                # solve the short lamberts problem
                V1_s, V2_s = lambert(R1, R2, TOF, "pro", mu)
                if np.any(np.isinf(V1_s)) or np.any(np.isinf(V2_s)):
                    print('%%%%%% Lambert Solver Failed, trying negative TOF')
                    V1_s, V2_s = lambert(R1, R2, -TOF, "pro", mu)

                # solve the long lamberts problem
                V1_l, V2_l = lambert(R1, R2, TOF, "retro", mu)
                if np.any(np.isinf(V1_l)) or np.any(np.isinf(V2_l)):
                    print('%%%%%% Lambert Solver Failed, trying negative TOF')
                    V1_l, V2_l = lambert(R1, R2, -TOF, "retro", mu)

                if missionTag == 'rendevous':
                    # Delta_v dep = what we need - what we have
                    dV_dep_s = np.linalg.norm(V1_s - V1)
                    dV_dep_l = np.linalg.norm(V1_l - V1)

                    # Delta_v arr = what we need - what we have
                    dV_arr_s = np.linalg.norm(V2 - V2_s)
                    dV_arr_l = np.linalg.norm(V2 - V2_l)

                    # choose lesser total dv
                    dV_tot_s = dV_dep_s + dV_arr_s
                    dV_tot_l = dV_dep_l + dV_arr_l
                elif missionTag == 'flyby':
                    # Delta_v dep = what we need - what we have
                    dV_dep_s = np.linalg.norm(V1_s - V1)
                    dV_dep_l = np.linalg.norm(V1_l - V1)

                    # Don't count arrival dv because just flying by
                    # choose lesser total dv
                    dV_tot_s = dV_dep_s
                    dV_tot_l = dV_dep_l

                dV_store = min([dV_tot_s, dV_tot_l])
            else:
                dV_store = np.nan

            # Store result
            grid_vel[ii][jj] = dV_store
            print("... [%i / %i] [%i / %i] Complete, dv = %.3f km/s" % (ii + 1, num_dep, jj + 1, num_arr, dV_store*149597870.7/86400))


    # Save Data
    my_data = {'dep_times': dep_times,
               'arr_times': arr_times,
               'grid_vel': grid_vel}

    output = open(os.path.join('.', 'data', 'data_' + tag + '_' + missionTag + '.pkl'), 'wb')
    pickle.dump(my_data, output)
    output.close()

    # Generate actual plot
    plot_porkchop(tag, False, cutoff, grid_vel, dep_times, arr_times, missionTag)

    return grid_vel


def plot_porkchop(tag, load_tag, cutoff, grid_vel=None, dep_times=None, arr_times=None, missionTag = 'Mission'):
    """

    :param grid_vel:
    :param dep_times:
    :param arr_times:
    :return:
    """

    # load data if not specified
    if load_tag:
        pkl_file = open(os.path.join('.', 'data', 'data_' + tag + '_' + missionTag + '.pkl'), 'rb')
        data1 = pickle.load(pkl_file)
        pkl_file.close()

        grid_vel = data1.get("grid_vel")
        dep_times = data1.get("dep_times")
        arr_times = data1.get("arr_times")

    X, Y = np.meshgrid(arr_times, dep_times)

    rows, cols = grid_vel.shape  # Convert grid vel to km/s for plotting
    grid_vel_plot = np.zeros((rows, cols))
    for ii in np.arange(rows):
        for jj in np.arange(cols):
            grid_vel_plot[ii][jj] = grid_vel[ii][jj] * 149597870.7 / 86400
            if grid_vel_plot[ii][jj] >= cutoff+1:  # To save some numbers around the cutoff for plotting contours
                grid_vel_plot[ii][jj] = np.nan
            if grid_vel_plot[ii][jj] <= 0:
                grid_vel_plot[ii][jj] = np.nan

    fig, ax = plt.subplots(1,1)
    plt.grid()

    cp = ax.contourf(Y, X, grid_vel_plot, cmap='jet', levels=np.arange(np.floor(np.nanmin(grid_vel_plot)), cutoff+1, 1))
    levels = np.floor(np.linspace(np.floor(np.nanmin(grid_vel_plot)), cutoff, 7))
    CS = ax.contour(Y, X, grid_vel_plot, levels, colors='k')
    ax.clabel(CS, fontsize=6, inline=True)

    clb = fig.colorbar(cp)
    clb.set_label('$\Delta v$ [km/s]')

    plt.title('%s, %s missions' % (tag, missionTag))
    plt.ylabel('Arrival Time')
    plt.xlabel('Departure Time')

    # Optional to cutoff plot to make easier to read
    cut_ax = False
    if cut_ax:
        yind = 150
        xind = 400
        ax.set_ylim([arr_times[0], arr_times[yind]])
        ax.set_xlim([dep_times[0], dep_times[xind]])

    # Make time ticks in MM/DD/YYYY format
    lA = len(arr_times)
    if cut_ax:
        lA = yind

    yticks = []
    for ii in range(lA):
        t = Time(arr_times[ii], format='jd')
        date_time = t.datetime.strftime("%m/%d/%Y")
        yticks.append(date_time)

    lD = len(dep_times)
    if cut_ax:
        lD = xind

    xticks = []
    for ii in range(lD):
        t = Time(dep_times[ii], format='jd')
        date_time = t.datetime.strftime("%m/%d/%Y")
        xticks.append(date_time)

    # Downsample if there are more than 4 ticks per axis
    num_ticks_y = 5
    indsA = np.arange(0, lA+1, 1, dtype=int)
    if lA >= num_ticks_y:
        stepA = np.floor(lA / num_ticks_y)
        indsA = np.arange(0, lA+1, stepA, dtype=int)
        indsA[-1] = indsA[-1] - 1

    num_ticks_x = 4
    indsD = np.arange(0, lD+1, 1, dtype=int)
    if lD >= num_ticks_x:
        stepD = np.ceil(lD / num_ticks_x)
        indsD = np.arange(0, lD+1, stepD, dtype=int)
        indsD[-1] = indsD[-1]-1

    ax.set_xticks([dep_times[i] for i in indsD])
    ax.set_yticks([arr_times[i] for i in indsA])

    ax.set_xticklabels([xticks[i] for i in indsD])
    if cut_ax:
        ax.set_yticklabels([yticks[i] for i in indsA-1])
    else:
        ax.set_yticklabels([yticks[i] for i in indsA])

    plt.tight_layout()
    plt.savefig(os.path.join('.', 'fig', 'pork_%s_%s.png' % (tag, missionTag)))
    plt.show()

