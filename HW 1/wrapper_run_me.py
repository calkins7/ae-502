import numpy as np
import sympy as sym
from univ_var_prop import kepler_U, rv_from_r0v0
from lambert_solver import lambert
from rv2orbele import Cart2Kep, Kep2Cart, getF
from porkchop_plots import generate_porkchop_plot, plot_porkchop
from datetime import datetime
import pandas as pd
from astropy.time import Time

def main():
    run_33test = False  # Test UVP, Curtis Example 3.6
    run_34test = False  # Test UVP Cart prop, Curtis Example 3.7
    run_lamberttest = False  # Test Lambert Solver, Curtis Example 5.2
    run_test_orb_eles = False  # Test conversion of cartesian to orbital elements and vice versa

    run_prob3 = False
    run_prob4 = False
    run_prob5 = True
    run_plot = False

    # Givens
    t0 = 2457754.50000 # JD, https://www.aavso.org/jd-calculator

    # 1I
    r1I = np.array([3.515868886595499*10**(-2),-3.162046390773074, 4.493983111703389]) # AU
    v1I = np.array([-2.317577766980901*10**(-3), 9.843360903693031*10**(-3),-1.541856855538041*10**(-2)]) # AU/day

    # 2I
    r2I = np.array([7.249472033259724, 14.61063037906177, 14.24274452216359]) # AU
    v2I = np.array([-8.241709369476881*10**(-3),-1.156219024581502*10**(-2),-1.317135977481448*10**(-2)]) # AU/day

    # Earth
    rE = np.array([-1.796136509111975 *10**(-1), 9.667949206859814*10**(-1),-3.668681017942158*10**(-5)])
    vE = np.array([-1.720038360888334*10**(-2),-3.211186197806460*10**(-3), 7.92773673596084*10**(-7)])

    # mu sun in AU^3/day^2
    mu = 132712000000 * (6.6846e-9/1)**3 * (86400/1)**2


    if run_33test:
        mu = 398600
        R = 10000

        # [a, ecc_norm, inc, RAAN, AoP, h, truAnom]
        eles = [-19655, 1.4682, 0, 0, 0, 95154, np.deg2rad(30)]

        chi = kepler_U(60*60, R, eles, 1/eles[0], mu, 10e-6, 10)
        print(chi)

    if run_34test:
        R0 = np.array([7000, -12124, 0])
        V0 = np.array([2.6679, 4.6210, 0])
        mu = 398600

        # rv_from_r0v0(R0, V0, t, mu, error, nMax, eles)
        eles = Cart2Kep(R0, V0, mu)
        R,V = rv_from_r0v0(R0, V0, 60*60, 398600, 10e-6, 10, eles)
        print(R)
        print(V)

    if run_lamberttest:
        R1 = np.array([5000, 10000, 2100]) # km
        R2 = np.array([-14600, 2500, 7000]) # km
        mu = 398600
        string = "pro"
        t = 60*60

        V1, V2 = lambert(R1, R2, t, string, mu)
        print(V1)
        print(V2)

        eles = Cart2Kep(R1, V1, mu)
        print(eles)

    if run_test_orb_eles:
        r = np.array([-6045, -3490, 2500])
        v = np.array([-3.457, 6.618, 2.533])
        mu = 398600
        eles = Cart2Kep(r, v, mu)
        # elements = [a, e, incl, RA, w, h, TA]
        print(eles)

        # elements = [a, e, incl, RA, w, h, TA]
        eles = np.array([10, 1.4, np.deg2rad(30), np.deg2rad(40), np.deg2rad(60),  80000, np.deg2rad(30)])
        r, v = Kep2Cart(eles, mu)
        print(r)
        print(v)


    if run_prob3:
        # For Interstellar Object 1
        # Get orbEles of Earth and Interstellar Object 1
        # [a, ecc_norm, inc, RAAN, AoP, h, truAnom]
        elesE = Cart2Kep(rE, vE, mu)
        eles1 = Cart2Kep(r1I, v1I, mu)

        # Get departure and arrival grid
        time_res = 1  # days
        dep_start = t0  # JD
        # https://www.aavso.org/jd-calculator
        dep_end = 2458118.50000  # Dec 31 2017 00:00:00 UTC
        arr_start = 2457966.50000  # Aug 1 2017 00:00:00 UTC
        arr_end = 2458514.50000  # Jan 31 2019 00:00:00 UTC

        dep_times = np.arange(dep_start, dep_end, time_res)
        arr_times = np.arange(arr_start, arr_end, time_res)

        tag = "1I Oumouamoua"
        kepler_U(arr_times[1] - t0, r1I, eles1, 1 / eles1[0], mu, 1e-6, 1000, r1I, v2I)

        # generate_porkchop_plot(dep_times, arr_times, rE, vE, r1I, v1I, t0, elesE, eles1, mu, tag, 50, 'rendevous')
        # plot_porkchop(tag, True, 20)
        generate_porkchop_plot(dep_times, arr_times, rE, vE, r1I, v1I, t0, elesE, eles1, mu, tag, 20, 'flyby')

    if run_prob4:
        # For Interstellar Object 1
        # Get orbEles of Earth and Interstellar Object 1
        # [a, ecc_norm, inc, RAAN, AoP, h, truAnom]
        elesE = Cart2Kep(rE, vE, mu)
        eles2 = Cart2Kep(r2I, v2I, mu)

        # Get departure and arrival grid
        time_res = 2 # days
        dep_start = t0  # JD
        # https://www.aavso.org/jd-calculator
        dep_end = 2459061.50000 # Jul 31 2020 00:00:00 UTC
        arr_start = 2458635.50000  # June 1 2019 00:00:00 UTC
        arr_end = 2459610.50000 # Jan 31 2022 00:00:00 UTC

        dep_times = np.arange(dep_start, dep_end, time_res)
        arr_times = np.arange(arr_start, arr_end, time_res)

        tag = "2I Borisov"
        generate_porkchop_plot(dep_times, arr_times, rE, vE, r2I, v2I, t0, elesE, eles2, mu, tag, 60, 'rendevous')
        # generate_porkchop_plot(dep_times, arr_times, rE, vE, r2I, v2I, t0, elesE, eles2, mu, tag, 20, 'flyby')

    if run_plot:
        # plot_porkchop('1I Oumouamoua', True, 20, missionTag='flyby')
        plot_porkchop('1I Oumouamoua', True, 50, missionTag='rendevous')
        # plot_porkchop('2I Borisov', True, 60, missionTag='rendevous')
        # plot_porkchop('2I Borisov', True, 20, missionTag='flyby')

    if run_prob5:
        eles1 = Cart2Kep(r1I, v1I, mu)
        eles2 = Cart2Kep(r2I, v2I, mu)

        # Get q (radius of periapsis)
        q1 = eles1[0]*(1 - eles1[1])
        q2 = eles2[0]*(1 - eles2[1])

        # Get time of pericenter passage (theta = 0)
        # Figure out required dF for each to get F = 0 (periapsis)
        dF1 = getF(eles1)
        dF2 = getF(eles2)
        #dF = chi/np.sqrt(-a)
        chi1_req = dF1*np.sqrt(-eles1[0])
        chi2_req = dF2 * np.sqrt(-eles2[0])

        # Do UVP with guess and check to get the chi we want at periapsis
        chi1 = kepler_U(-3215, r1I, eles1, 1/eles1[0], mu, 1e-8, 100, r1I, v1I)
        chi2 = kepler_U(-17525, r2I, eles2, 1 / eles2[0], mu, 1e-8, 500, r2I, v2I)

        t01 = Time(t0 -3215, format='jd')
        date_time01 = t01.datetime.strftime("%m/%d/%Y")

        t02 = Time(t0 - 17525, format='jd')
        date_time02 = t02.datetime.strftime("%m/%d/%Y")

        # [a, ecc_norm, inc, RAAN, AoP, h, truAnom]
        df = pd.DataFrame(dict(Name=["1I 'Oumouamoua", "2I Borisov"],
                               q=[q1, q2],
                               e=[eles1[1], eles2[1]],
                               inc=np.rad2deg([eles1[2], eles2[2]]),
                               RAAN=np.rad2deg([eles1[3], eles2[3]]),
                               AoP=np.rad2deg([eles1[4], eles2[4]]),
                               T0=[date_time01, date_time02]))
        df.style.format({
            ("Numeric", "Integers"): '\${}',
            ("Numeric", "Floats"): '{:.3f}',
            ("Non-Numeric", "Strings"): str.upper
        })
        print(df.style.to_latex())


if __name__ == "__main__":
    main()