import numpy as np
from gaussPlanetaryEquations import intGaussPlanetaryEq
import matplotlib.pyplot as plt
from rv2orbele import getM
import os


def main():
    tag_runCurtisCheck = False
    tag_runHW = True

    if tag_runCurtisCheck:
        rp = 6678  # km
        ra = 9940  # km
        RAAN = np.deg2rad(45)  # rad
        i = np.deg2rad(28)  # rad
        AoP = np.deg2rad(30)  # rad
        theta = np.deg2rad(40)  # rad

        GM = 3.986 * 10 ** 5  # km3/s2
        J2 = 0.00108
        R = 6370  # km

        a = (rp + ra)/2
        e = (ra - rp)/(ra + rp)
        h = np.sqrt(GM * a * (1 - e ** 2))

        eles = [a, e, i, RAAN, AoP, h, theta]

        # Get M0
        M0 = getM(eles)

        # Time
        hrs2sec = 60 * 60
        tEnd = 48 * hrs2sec  # s
        dT = 10  # s

        ele_IC = [a, e, i, RAAN, AoP, h, M0]
        # Returns [da_dt, de_dt, di_dt, dRAAN_dt, dAoP_dt, dM_dt]
        dEles_dt = intGaussPlanetaryEq(ele_IC, GM, J2, R, tEnd, dT)

        # plot dEles_dt on subplots
        t = np.arange(0, tEnd, dT)
        fig, axs = plt.subplots(5, 1)
        fig.set_size_inches(10, 10)
        # SMA
        axs[0].plot(t/hrs2sec, dEles_dt[0])
        axs[0].set_ylabel("a [km]")
        # Eccentricity
        axs[1].plot(t/hrs2sec, dEles_dt[1])
        axs[1].set_ylabel("e")
        # Inclination
        axs[2].plot(t/hrs2sec, np.rad2deg(dEles_dt[2]))
        axs[2].set_ylabel("i [deg]")
        # RAAN
        axs[3].plot(t/hrs2sec, np.rad2deg(dEles_dt[3]))
        axs[3].set_ylabel("$\Omega$")
        # AoP
        axs[4].plot(t/hrs2sec, np.rad2deg(dEles_dt[4]))
        axs[4].set_ylabel("$\omega$")
        axs[4].set_xlabel("Time [hrs]")
        for ax in axs:
            ax.grid()
        # turn off x tick labels for all but bottom plot
        for ax in axs[:-1]:
            ax.set_xticklabels([])
        plt.tight_layout()
        plt.show()


    if tag_runHW:
        # Givens
        a = 26600  # km
        i = 1.10654  # rad
        e = 0.74
        AoP = np.deg2rad(5)  # rad
        RAAN = np.deg2rad(90)  # rad
        M0 = np.deg2rad(10)  # rad
        GM = 3.986 * 10 ** 5  # km3/s2.
        J2 = 0.00108
        R = 6370  # km
        T = 2*np.pi*np.sqrt(a**3/GM)  # s
        n = 2*np.pi/T

        # Time
        hrs2sec = 60 * 60
        day2sec = 24 * hrs2sec
        tEnd = 10*day2sec # s
        dT = 2*60  # s

        h = np.sqrt(GM*a*(1-e**2))
        ele_IC = [a, e, i, RAAN, AoP, h, M0]
        # Returns [da_dt, de_dt, di_dt, dRAAN_dt, dAoP_dt, dM_dt]
        dEles_dt, time = intGaussPlanetaryEq(ele_IC, GM, J2, R, tEnd, dT)

        # little omega trend
        little_omega_slope = (3/4*n*J2*R**2/a**2*(5*np.cos(i)**2 - 1)/(1-e**2)**2)
        Big_omega_slope = -3 / 2 * n * J2 * (R / a) ** 2 * np.cos(i) / (1 - e ** 2) ** 2

        little_omega_trend = little_omega_slope*time + np.deg2rad(5.025)
        Big_omega_trend = Big_omega_slope*time + RAAN

        # plot dEles_dt on subplots
        t = np.arange(0, tEnd, dT)
        fig, axs = plt.subplots(5, 1)
        axs[0].plot(t/day2sec, dEles_dt[0])
        axs[0].set_ylabel("a [km]")
        axs[1].plot(t/day2sec, dEles_dt[1])
        axs[1].set_ylabel("e")
        axs[2].plot(t/day2sec, np.rad2deg(dEles_dt[2]))
        axs[2].set_ylabel("i [deg]")
        axs[3].plot(t/day2sec, np.rad2deg(dEles_dt[3]))
        # plot trend line
        axs[3].plot(t/day2sec, np.rad2deg(Big_omega_trend))
        axs[3].legend(["$\Omega$", "$\Omega$ secular solution"])
        axs[3].set_ylabel("$\Omega$ [deg]")
        axs[4].plot(t/day2sec, np.rad2deg(dEles_dt[4]))
        # plot trend line
        axs[4].plot(t / day2sec, np.rad2deg(little_omega_trend))
        axs[4].legend(["$\omega$", "$\omega$ secular solution"])
        axs[4].set_ylabel("$\omega$ [deg]")
        axs[4].set_xlabel("Time [days]")
        fig.tight_layout(pad=1.0)
        for ax in axs:
            ax.grid()
            ax.autoscale(enable=True, axis='x', tight=True)
        # turn off x tick labels for all but bottom plot
        for ax in axs[:-1]:
            ax.set_xticklabels([])
        # make figure bigger
        fig.set_size_inches(10, 10)
        plt.subplots_adjust(left=0.1,
                            bottom=0.075,
                            right=0.95,
                            top=0.99,
                            wspace=0.4,
                            hspace=0.2)

        plt.savefig(os.path.join("./figs", "prob3_10days.png"))
        plt.show()





if __name__ == "__main__":
    main()