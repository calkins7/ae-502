import numpy as np
import matplotlib.pyplot as plt
import os

def main():
    # Givens
    J2 = 0.00196
    R = 3390  # km
    GM = 4.282 * 10**4  # km3 / s2
    T = 24*60*60 + 39*60 + 35   # seconds

    rps = np.arange(400, 20446.67, 10)  # km
    a = (GM * T ** 2 / (4 * np.pi ** 2)) ** (1 / 3)  # km
    es = 1 - rps / a
    i = np.arccos(np.sqrt(1 / 5))
    n = 2 * np.pi / T
    Omega_bar_dots = -3 / 2 * n * J2 * (R/a)**2 * np.cos(i) / (1 - es ** 2) ** 2

    # Find Omega_bar_dot where precession rate is 360 deg/year
    Omega_bar_dot_req = 180 * np.pi / 180 / (60 * 60 * 24 * 365)
    # get index where where the omega_bar_dot are less than this
    idx = np.where(abs(Omega_bar_dots) < Omega_bar_dot_req)[0][0]

    # Choose Rp where this happens
    rp_choose = rps[idx]
    a = (GM * T ** 2 / (4 * np.pi ** 2)) ** (1 / 3)  # km
    e = 1 - rp_choose / a
    i = np.arccos(np.sqrt(1 / 5))
    i_neg = np.arccos(-np.sqrt(1 / 5))
    n = 2 * np.pi / T
    Omega_bar_dot = -3 / 2 * n * J2 * (R / a) ** 2 * np.cos(i) / (1 - e ** 2) ** 2
    ra = a * (1 + e)
    print("Choose r_p [km]: ", rp_choose)
    print("SMA [km]: ", a)
    print("Eccentricity: ", e)
    print("Inclination [deg]: ", np.rad2deg(i), " or ", np.rad2deg(i_neg))
    print("RAAN rate [rad/s]: ", Omega_bar_dot, " or ", -Omega_bar_dot)
    print("RAAN rate [deg/yr]: ", abs(Omega_bar_dot)*60*60*24*365*180/(np.pi))
    print("r_a [km]: ", ra)

    # plot Rp and eccentricity vs. Omega_bar_dot
    # Make subplot for Rp
    fig, axs = plt.subplots(2)
    fig.suptitle("Problem 2")
    axs[0].plot(rps, Omega_bar_dots * 60 * 60 * 24 * 365 * 180 / (np.pi))
    axs[0].set_xlabel("Radius of Periapsis [km]")
    axs[0].set_ylabel(r"$\dot{\bar{\Omega}}$ [deg/yr]")
    # make a vertical line at x = rp_choose
    axs[0].axvline(x=rp_choose, color='r')
    axs[0].grid()
    axs[0].legend([r"$\dot{\bar{\Omega}}$", "r_p chosen"])
    # make subplot for eccentricity versus Omega_bar_dot
    axs[1].plot(es, Omega_bar_dots * 60 * 60 * 24 * 365 * 180 / (np.pi))
    axs[1].set_xlabel("Eccentricity")
    axs[1].set_ylabel(r"$\dot{\bar{\Omega}}$ [deg/yr]")
    # make a vertical line at x = rp_choose
    axs[1].axvline(x=e, color='r')
    axs[1].grid()
    axs[1].legend([r"$\dot{\bar{\Omega}}$", "e chosen"])
    # Increase vertical padding between subplots
    fig.tight_layout(pad=1.0)

    plt.savefig(os.path.join("./figs", "prob2.png"))
    plt.close()




if __name__ == "__main__":
        main()