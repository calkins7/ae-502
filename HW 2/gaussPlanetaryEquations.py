import numpy as np
from scipy.integrate import ode
from rv2orbele import Kep2Cart, getEfromM, getThetaFromE
import matplotlib.pyplot as plt

def intGaussPlanetaryEq(ele_IC, GM, J2, R, tEnd, dT):
    """
    Integrate Gauss Planetary Equations and return the variation in elements
    returned elements [da_dt, de_dt, di_dt, dRAAN_dt, dAoP_dt, dM_dt]

    :param ele_IC: initial elements [a, e, i, RAAN, AoP, h, M0]
    :param GM: gravitational parameter
    :param J2: perturbation
    :param R: radius of the planet
    :param tEnd: final time to propagate to
    :param dT: time step
    :return dEles_dt: array of variation in elements over time in
            [da_dt, de_dt, di_dt, dRAAN_dt, dAoP_dt, dM_dt]
    :return time: array of time steps
    """
    # Unpack initial condition elements
    a, e, i, RAAN, AoP, h, M0 = ele_IC

    # Convert M to theta
    E0 = getEfromM(ele_IC, M0)
    theta0 = getThetaFromE(ele_IC, E0)

    # Define initial state
    int_eles0 = [h, e, theta0, RAAN, i, AoP]
    T0 = 0

    # Solve Gauss equations, set up helper function for integrator
    def derivFcn(t, x):
        return gpeODE(t, x, GM, J2, R)

    # Define integrator, set tolerances and the initial state
    rv = ode(derivFcn)
    rtol = 1e-14
    atol = 1e-20
    rv.set_integrator('dopri5', rtol=rtol, atol=atol)
    print("Integrator relative tolerance is " + "{:.2E}".format(rtol))
    print("Integrator absolute tolerance is " + "{:.2E}".format(atol) + "\n")
    rv.set_initial_value(int_eles0, T0)

    # Define output array
    output = np.zeros((len(np.arange(0, tEnd, dT)), 7))
    output[0, :] = np.append(int_eles0, 0)

    # Run the integrator and populate
    counter = 1
    while rv.successful() and rv.t < tEnd-dT:
        rv.integrate(rv.t + dT)
        output[counter, :] = [rv.y[0], rv.y[1], rv.y[2], rv.y[3], rv.y[4], rv.y[5], rv.t]
        counter += 1
        if (counter % 500) == 0:
            print(f'Integration Step {counter} / {len(np.arange(0, tEnd, dT))}')

    #  output a numpy array [h, e, theta0, RAAN, i, AoP]
    output = np.array(output)

    #plot output
    # hrs2sec = 60 * 60
    # t = np.arange(0, tEnd, dT)
    # fig, axs = plt.subplots(6, 1)
    # fig.set_size_inches(10, 10)
    # axs[0].plot(t / hrs2sec, output[:,0])
    # axs[0].set_ylabel("h")
    # axs[1].plot(t / hrs2sec, output[:,1])
    # axs[1].set_ylabel("e")
    # axs[2].plot(t / hrs2sec, output[:,2])
    # axs[2].set_ylabel("theta")
    # axs[3].plot(t / hrs2sec, output[:,3])
    # axs[3].set_ylabel("RAAN")
    # axs[4].plot(t / hrs2sec, output[:,4])
    # axs[4].set_ylabel("i")
    # axs[5].plot(t / hrs2sec, output[:,5])
    # axs[5].set_ylabel("AoP")
    # plt.show()


    # convert from int_eles [h, e, theta0, RAAN, i, AoP]
    # to [a, e, i, RAAN, AoP, h, M0]
    da_dt = getSMAVar(output, GM)
    de_dt = output[:, 1]
    di_dt = output[:, 4]
    dRAAN_dt = output[:, 3]
    dAoP_dt = output[:, 5]
    dM_dt = getMVar(output)
    dEles_dt = [da_dt, de_dt, di_dt, dRAAN_dt, dAoP_dt, dM_dt]

    time = output[:, 6]

    return dEles_dt, time


def gpeODE(t, int_eles, GM, J2, R):
    """
    Gauss Planetary Equations of Motion, Curtis Eqn 12.89
    :param t:
    :param int_eles: [h, e, theta0, RAAN, i, AoP]
    :param GM: gravitational parameter
    :param J2: J2 perturbation factor
    :param R: planet's radius
    :return dX: [dh_dt, de_dt, dtheta_dt, dRAAN_dt, di_dt, dAoP_dt]
    """
    dX = np.zeros([6, 1])

    # Unpack elements
    h, e, theta, RAAN, i, AoP = int_eles

    # Helper Calculations
    u = AoP + theta
    r = h**2/GM/(1+e*np.cos(theta))

    # Get dX per Curtis Eqn 12.89
    dh_dt = -3/2*J2*GM*R**2/r**3*np.sin(i)**2*np.sin(2*u)
    de_dt = 3/2*J2*GM*R**2/(h*r**3)*(h**2/(GM*r)*np.sin(theta)*(3*np.sin(i)**2*np.sin(u)**2-1) - np.sin(2*u)*np.sin(i)**2*((2 + e*np.cos(theta))*np.cos(theta) + e))
    dtheta_dt = h/r**2 + 3/2*J2*GM*R**2/(e*h*r**3)*(h**2/(GM*r)*np.cos(theta)*(3*np.sin(i)**2*np.sin(u)**2 - 1) + (2+e*np.cos(theta))*np.sin(2*u)*np.sin(i)**2*np.sin(theta))
    dRAAN_dt = -3*J2*GM*R**2/(h*r**3)*np.sin(u)**2*np.cos(i)
    di_dt = -3/4*J2*GM*R**2/(h*r**3)*np.sin(2*u)*np.sin(2*i)
    dAoP_dt = 3/2*J2*GM*R**2/(e*h*r**3)*(h**2/(GM*r)*np.cos(theta)*(1-3*np.sin(i)**2*np.sin(u)**2)-(2+e*np.cos(theta))*np.sin(2*u)*np.sin(i)**2*np.sin(theta) + 2*e*np.cos(i)**2*np.sin(u)**2)

    dX[0] = dh_dt
    dX[1] = de_dt
    dX[2] = dtheta_dt
    dX[3] = dRAAN_dt
    dX[4] = di_dt
    dX[5] = dAoP_dt

    return dX


def getSMAVar(ele_int, GM):
    """
    Curtis Equation 2.72
    :param ele_int: [h, e, theta0, RAAN, i, AoP] over time
    :param GM: gravitational parameter
    :return da_dt: array of SMA variation over time
    """
    # Unpack elements
    h = ele_int[:, 0]
    e = ele_int[:, 1]

    # Calculate SMA variation
    da_dt = h**2/GM/(1-e**2)

    return da_dt


def getMVar(ele_int):
    """
    Curtis Equation 3.6
    :param ele_int: [h, e, theta, RAAN, i, AoP] over time
    :return dM_dt: array of mean anomaly variation over time
    """
    # Unpack elements
    e = ele_int[:, 1]
    theta = ele_int[:, 2]

    # Calculate Mean Anomaly variation
    dM_dt = 2*np.arctan2(np.sqrt(1-e)*np.sin(theta/2), np.sqrt(1+e)) - e*np.sqrt(1-e**2)*np.sin(theta)/(1+e*np.cos(theta))

    return dM_dt
