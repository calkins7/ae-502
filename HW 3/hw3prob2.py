import numpy as np
from scipy.integrate import ode
from rv2orbele import Kep2Cart, getEfromM, getThetaFromE
import matplotlib.pyplot as plt

def delaunay(t, x, omega):
    L = x[0]
    dX = np.zeros([6, 1])

    # Ldot
    dX[3] = 1/(L**3)
    # hdot
    dX[5] = omega

    return dX

def main():
    a = 1
    e = 0.5
    inc = np.deg2rad(45)
    omega = 0.01

    n = np.sqrt(1/a**3)
    L = n*a**2
    G = L*(1-e**2)**0.5
    H = G*np.cos(inc)
    l = 0
    g = 0
    h = 0
    int_eles0 = [L, G, H, l, g, h]

    # set up integration
    tEnd = 100
    T0 = 0
    dT = 0.01
    def derivFcn(t, x):
        return delaunay(t, x, omega)

    # Define integrator, set tolerances and the initial state
    rv = ode(derivFcn)
    rtol = 1e-14
    atol = 1e-20
    rv.set_integrator('dopri5', rtol=rtol, atol=atol)
    print("Integrator relative tolerance is " + "{:.2E}".format(rtol))
    print("Integrator absolute tolerance is " + "{:.2E}".format(atol) + "\n")
    rv.set_initial_value(int_eles0, T0)

    # Define output array
    output = np.zeros((len(np.arange(0, tEnd, dT)), 7))
    output[0, :] = np.append(int_eles0, 0)

    # Run the integrator and populate
    counter = 1
    while rv.successful() and rv.t < tEnd - 2*dT:
        rv.integrate(rv.t + dT)
        output[counter, :] = [rv.y[0], rv.y[1], rv.y[2], rv.y[3], rv.y[4], rv.y[5], rv.t]
        counter += 1

    # Convert Delaunay variables to Keplerian
    ang_momentum = np.sqrt( a * (1 - e ** 2))
    rs = np.zeros((len(output), 3))
    for ii in range(len(output)):
        # get current RAAN (h) from output
        RAAN = output[ii, 5]
        # set up eles [a, e, incl, RA, w, h, TA]
        eles = [a, e, inc, RAAN, g, ang_momentum, 0]

        # Get true anomaly from mean anomaly
        M = output[ii, 3] # little l is mean anomaly
        E = getEfromM(eles, M)
        theta = getThetaFromE(eles, E)
        eles[6] = theta

        # Convert to cartesian
        r, v = Kep2Cart(eles, mu=1)
        rs[ii, :] = r

    # Plot rs in 3 dimensions
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(rs[:, 0], rs[:, 1], rs[:, 2])
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.savefig('hw3prob2.png')
    plt.show()

if __name__ == "__main__":
    main()

