from numpy import sqrt, sin, cos, cosh, sinh, nan

def stumpC(z):
    '''
    This function evaluates the Stumpff function C(z) according
    to Equation 3.53.
    :param z: input argument
    :return: c - value of C(z)
    '''
    if z > 0.:
        c = (1 - cos(sqrt(z))) / z
    elif z < 0.:
        c = (cosh(sqrt(-z)) - 1) / (-z)
    else:
        c = 1 / 2
    return c


def stumpS(z):
    '''
    This function evaluates the Stumpff function S(z) according
    to Equation 3.52.
    :param z: input argument
    :return: s - value of S(z)
    '''
    if z > 0.:
        s = (sqrt(z) - sin(sqrt(z)))/(sqrt(z))**3
    elif z < 0.:
        s = (sinh(sqrt(-z)) - sqrt(-z))/(sqrt(-z))**3
    else:
        s = 1/6

    return s