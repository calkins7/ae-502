import numpy as np
import math as m

# Author Grace Calkins
# Code from Spacecraft Dynamics, Fall 2019, UT Austin

def robustAcos(arg):
    if abs(arg-1.0) < 1e-12 and arg > 1.0:
        arg = 1.0
    elif abs(arg+1.0) and arg < -1.0:
        arg = -1.0
    angle = np.arccos(arg)
    return angle

def rad2deg(rad):
    deg = rad*180/np.pi
    return deg

def deg2rad(deg):
    rad = deg/180*np.pi
    return rad

def Cart2Kep (R, V, mu):
    # Adapted from D.18 Curtis

    # make sure R and V are arrays
    R = np.array(R)
    V = np.array(V)

    eps = 1.e-10
    r = np.linalg.norm(R)
    v = np.linalg.norm(V)
    vr = np.dot(R, V) / r
    H = np.cross(R, V)
    h = np.linalg.norm(H)

    # Equation 4.7:
    incl = robustAcos(H[2] / h)
    # Equation4.8:
    N = np.cross(np.array([0, 0, 1]), H)
    n = np.linalg.norm(N)
    # Equation 4.9:
    if n != 0:
        RA = robustAcos(N[0] / n)
        if N[2] < 0:
            RA = 2 * np.pi - RA
    else:
        RA = 0

    # Equation 4.10:
    E = 1 / mu * ((v ** 2 - mu / r) * R - r * vr * V)
    e = np.linalg.norm(E)

    # Equation 4.12 (incorporating the case e = 0):
    if n != 0:
        if e > eps:
            w = robustAcos(np.dot(N, E) / n / e)
            if E[2] < 0:
                w = 2 * np.pi - w
        else:
            w = 0
    else:
        w = 0

    # Equation 4.13a (incorporating the case e = 0):
    if e > eps:
        TA = robustAcos(np.dot(E, R) / e / r)
        if vr < 0:
            TA = 2 * np.pi - TA
    else:
        cp = np.cross(N, R)
        if cp[2] >= 0:
            TA = robustAcos(np.dot(N, R) / n / r)
        else:
            TA = 2 * np.pi - robustAcos(np.dot(N, R) / n / r)

    # Equation 4.62(a < 0 for a hyperbola):
    a = h ** 2 / mu / (1 - e ** 2)

    elements = [a, e, incl, RA, w, h, TA]
    return elements


def Kep2Cart (orbElems, mu):
    # Adapted from Curtis D.22

    h = orbElems[5]
    e = orbElems[1]
    RA = orbElems[3]
    incl = orbElems[2]
    w = orbElems[4]
    TA = orbElems[6]

    # Equations 4.45 and 4.46(rp and vp are column vectors):
    vec1 = np.array([[1], [0], [0]])
    vec2 = np.array([[0], [1], [0]])
    rp = (h ** 2 / mu) * (1 / (1 + e * np.cos(TA))) * (np.dot(np.cos(TA), vec1) + np.dot(np.sin(TA), vec2))
    vp = np.dot((mu / h), np.array([-np.sin(TA), e + np.cos(TA), 0]))

    # Equation 4.34:
    R3_W = np.array([[np.cos(RA), np.sin(RA), 0],
            [-np.sin(RA), np.cos(RA), 0],
            [0, 0, 1]])
    # Equation 4.32:
    R1_i = np.array([[1, 0, 0],
            [0, np.cos(incl), np.sin(incl)],
            [0, -np.sin(incl), np.cos(incl)]])
    # Equation 4.34:
    R3_w = np.array([[np.cos(w), np.sin(w), 0],
            [-np.sin(w), np.cos(w), 0],
            [0, 0, 1]])

    # Equation 4.49:
    Q_pX = np.transpose(np.dot(R3_w, np.dot(R1_i, R3_W)))
    # Equations 4.51(r and v are column vectors):
    r = np.dot(Q_pX, rp)
    v = np.dot(Q_pX, vp)

    # Convert r and v into row vectors:
    r = np.transpose(r)
    v = np.transpose(v)

    return r, v


def getE(eles):
    ecc = eles[1]
    truAnom = eles[6]
    # Curtis eqn 3.13b
    E = 2*np.arctan2(np.sqrt(1-ecc)*np.tan(truAnom/2), np.sqrt(1 + ecc))

    return E

def getF(eles):
    ecc = eles[1]
    truAnom = eles[6]

    # Curtis equation 3.39
    F = np.log((np.sqrt(ecc + 1) + np.sqrt(ecc - 1)*np.tan(truAnom/2)) / (np.sqrt(ecc + 1) - np.sqrt(ecc - 1)*np.tan(truAnom/2)))

    return F

def getM(eles):
    E = getE(eles)
    M = E - eles[1]*np.sin(E)
    return M

def getEfromM(eles, M):
    # Curtis algorithm 3.1
    ecc = eles[1]
    error = 1e-8

    if M < np.pi:
        E = M + ecc/2
    else:
        E = M - ecc/2

    ratio = 1
    while abs(ratio) > error:
        ratio = (E - ecc*np.sin(E) - M) / (1 - ecc*np.cos(E))
        E = E - ratio
    return E

def getThetaFromE(eles, E):
    # Curtis eqn 3.13a
    ecc = eles[1]
    theta = 2*np.arctan2(np.sqrt(1 + ecc)*np.sin(E/2), np.sqrt(1 - ecc)*np.cos(E/2))
    return theta